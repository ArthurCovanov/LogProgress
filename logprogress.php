<div class="progress"><!-- style="width:60%;">-->
	<?php
		$filename = './progress.log';
		$f = fopen($filename, 'r');
		$line = fgets($f);
		fclose($f);
		$progress = (int)$line;
		echo "<div id=\"progressbar\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"".$progress."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:".$progress."%;\">".$progress."%</div>";
	?>
</div>
