var i = 0;

$().ready(function()
{
	var timer;
	{
		$("#logDiv").load("readlog.php");
		$("#logProgress").load("logprogress.php");
		var $logdiv = $("#logDiv");
		var $logprogress = $("#logProgress");
		timer = setInterval(function()
		{
			i++;
			$logdiv.load("readlog.php");
			$logprogress.load("logprogress.php");
			var progressbar = document.getElementById("progressbar");
			var percentage = progressbar.innerHTML;
			percentage = percentage.replace(/[^0-9]+|\s+/gmi, ""); //Replace Everycharacter that is not a number
			//alert(percentage);
			if(percentage == 100)
				clearInterval(timer);
		}, 1000); // Refresh scores every seconds
	}
});
